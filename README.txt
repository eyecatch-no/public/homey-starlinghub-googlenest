Adds support for Google Nest devices via the Starling Home Hub.
Currently supports Nest Protect, Nest Thermostat and Nest Temperature Sensor.