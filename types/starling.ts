export interface StarlingResponse {
  status: string;
}

export interface StarlingDeviceResponse extends StarlingResponse {
  devices: StarlingDevice[];
}

export interface StarlingDevice {
  type: string;
  id: string;
  where: string;
  name: string;
  serialNumber: string;
  structureName: string;
  supportsStreaming: boolean;
}

export interface ProtectDeviceResponse extends StarlingResponse {
  properties: ProtectDevice;
}

export type ProtectDevice = {
  type: string;
  id: string;
  where: string;
  name: string;
  serialNumber: string;
  structureName: string;
  smokeDetected: boolean;
  coDetected: boolean;
  batteryStatus: string;
  manualTestActive: boolean;
  occupancyDetected: boolean;
};

export interface TemperatureSensorDeviceResponse extends StarlingResponse {
  properties: TemperatureSensorDevice;
}

export type TemperatureSensorDevice = {
  type: string;
  id: string;
  where: string;
  name: string;
  serialNumber: string;
  structureName: string;
  batteryStatus: string;
  currentTemperature: number;
};

export interface ThermostatDeviceResponse extends StarlingResponse {
  properties: ThermostatDevice;
}

export type ThermostatDevice = {
  type: string;
  id: string;
  where: string;
  name: string;
  serialNumber: string;
  structureName: string;
  batteryStatus: string;
  backplateTemperature: number;
  canCool: boolean;
  canHeat: boolean;
  currentHumidifierState: string;
  currentTemperature: number;
  displayTemperatureUnits: string;
  ecoMode: boolean;
  fanRunning: boolean;
  hotWaterEnabled: boolean;
  humidifierActive: boolean;
  humidityPercent: boolean;
  hvacState: string;
  hvacMode: string;
  presetSelected: string;
  sensorSelected: string;
  targetCoolingThresholdTemperature: number;
  targetHeatingThresholdTemperature: number;
  targetHumidity: number;
  targetTemperature: number;
  tempHoldMode: boolean;
};

export type PingResult = {
  status: string;
  productName: string;
  serviceName: string;
  serviceVersion: string;
  local_ip: string;
};

export type StatusResult = {
  apiVersion: number;
  apiReady: boolean;
  connectedToNest?: boolean;
  appName: string;
  timestamp?: Date;
  permissions: {
    read: boolean;
    write: boolean;
    camera: boolean;
  };
};

export type StarlingError = {
  status: string;
  code?: string;
  message?: string;
}

export interface SetFanRunningResponse extends StarlingResponse {
  setStatus: {
    fanRunning: string;
  }
}

export interface SetThermostatHvacModeResponse extends StarlingResponse {
  setStatus: {
    hvacMode: string;
  }
}

export interface SetThermostatEcoModeResponse extends StarlingResponse {
  setStatus: {
    ecoMode: string;
  }
}

export interface SetThermostatTemperatureResponse extends StarlingResponse {
  setStatus: {
    targetTemperature: string;
    targetCoolingThresholdTemperature: string;
    targetHeatingThresholdTemperature: string;
  }
}

