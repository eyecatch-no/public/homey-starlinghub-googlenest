const enum DeviceType {
  NestProtect = "protect",
  NestTemperatureSensor = "temp_sensor",
  NestThermostat = "thermostat",
}

export default DeviceType;
