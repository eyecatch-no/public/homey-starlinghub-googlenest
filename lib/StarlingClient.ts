import axios, { Axios, AxiosInstance, isAxiosError } from "axios";
import https from "https";
import Logger from "./Logger";
import {
  PingResult,
  ProtectDevice,
  ProtectDeviceResponse,
  TemperatureSensorDevice,
  TemperatureSensorDeviceResponse,
  ThermostatDevice,
  ThermostatDeviceResponse,
  StarlingDevice,
  StarlingDeviceResponse,
  StatusResult,
  SetFanRunningResponse,
  SetThermostatHvacModeResponse,
  SetThermostatEcoModeResponse,
  SetThermostatTemperatureResponse,
  StarlingError,
} from "../types/starling";
import DeviceType from "./DeviceType";
import StarlingDiscovery from './StarlingDiscovery';
import throttledQueue from 'throttled-queue';

export function isStarlingError(payload: any): payload is StarlingError {
  if (payload?.code) {
    return true;
  }

  return false;
}

class StarlingClient {
  private apiKey = "";
  private logger: Logger;
  private httpClient: AxiosInstance | null = null;

  constructor(apiKey: string, logger: Logger) {
    this.logger = logger;
    this.apiKey = apiKey;
  }

  public isInitialized: boolean = false;

  private getBaseUrl(ip: string): string {
    return `https://${ip}:3443/api/connect/v1/`;
  }

  async init(): Promise<PingResult> {
    const addr = await StarlingDiscovery.getInstance().getAddress();

    this.logger.info(`Initializing Starling Client with IP: ${addr}`);

    const r = await axios.get<PingResult>(
      `http://${addr}/api/starling/v1/ping`
    );

    if (r?.data.status == "OK") {
      const baseUrl = this.getBaseUrl(r.data.local_ip);

      this.httpClient = axios.create({
        baseURL: baseUrl,
        httpsAgent: new https.Agent({
          rejectUnauthorized: false,
        }),
      });

      this.httpClient.interceptors.request.use((config) => {
        config.params = {
          key: this.apiKey,
          ...config.params,
        };

        this.logger.debug(`Request: ${config.baseURL}${config.url} | Query Params: ${JSON.stringify(config.params)}`);

        return config;
      });

      this.isInitialized = true;
    }

    return r?.data;
  }

  async getStatus(): Promise<StatusResult | null> {
    const r = await this.httpClient?.get<StatusResult>("status");

    this.logger.info(`Retrieved Status: ${JSON.stringify(r?.data)}`);

    if (r) {
      return r.data;
    }

    return null;
  }

  async getDevices(deviceType: DeviceType | null): Promise<StarlingDevice[]> {
    const r = await this.httpClient?.get<StarlingDeviceResponse>("devices");

    this.logger.info(
      `Retrieved ${deviceType} Devices: ${JSON.stringify(r?.data)}`
    );

    if (r?.data?.devices) {
      return r.data.devices.filter((x) => !deviceType || x.type == deviceType);
    }

    return [];
  }

  async getProtectDevice(id: string): Promise<ProtectDevice | null> {
    const r = await this.httpClient?.get<ProtectDeviceResponse>(
      `devices/${id}`
    );

    this.logger.debug(`Retrieved ${id}: ${JSON.stringify(r?.data)}`);

    return r?.data?.properties || null;
  }

  async getTemperatureSensorDevice(id: string): Promise<TemperatureSensorDevice | null> {
    const r = await this.httpClient?.get<TemperatureSensorDeviceResponse>(
      `devices/${id}`
    );

    this.logger.debug(`Retrieved ${id}: ${JSON.stringify(r?.data)}`);

    return r?.data?.properties || null;
  }

  async getThermostatDevice(id: string): Promise<ThermostatDevice | null> {
    const r = await this.httpClient?.get<ThermostatDeviceResponse>(
      `devices/${id}`
    ); 
      
    this.logger.debug(`Retrieved ${id}: ${JSON.stringify(r?.data)}`);
      
    return r?.data?.properties || null;
  }

  async setThermostatFan(id: string, fanMode: boolean) {
    return await this.throttlePost(async () => {
      const r = await this.httpClient?.post<SetFanRunningResponse>(
        `devices/${id}`,
        {
          fanRunning: fanMode
        }
      );

      this.logger.info(`Retrieved ${id}: ${JSON.stringify(r?.data)} {"fanRunning": ${fanMode}}`);
      
      return r?.data?.status == "OK" &&
        r?.data?.setStatus?.fanRunning == "OK";
    });
  }

  async setThermostatHvacMode(id: string, mode: string) {
    return await this.throttlePost(async () => {
      const r = await this.httpClient?.post<SetThermostatHvacModeResponse>(
        `devices/${id}`,
        { 
          hvacMode: mode
        }
      );

      this.logger.info(`Retrieved ${id}: ${JSON.stringify(r?.data)} {"hvacMode": ${mode}}`);
      
      return r?.data?.status == "OK" &&
        r?.data?.setStatus?.hvacMode == "OK";
    });
  }

  async setThermostatEcoMode(id: string, mode: boolean) {
    return await this.throttlePost(async () => {
      const r = await this.httpClient?.post<SetThermostatEcoModeResponse>(
        `devices/${id}`,
        {
          ecoMode: mode
        } 
      );

      this.logger.info(`Retrieved ${id}: ${JSON.stringify(r?.data)} {"ecoMode": ${mode}}`);

      return r?.data?.status == "OK" &&
        r?.data?.setStatus?.ecoMode == "OK";
    });
  } 

  async setThermostatTemperature(id: string, targetTemperature: number | null, targetHeatingTemperature: number | null, targetCoolingTemperature: number | null) {
    const payload: { [key: string]: number } = {};
    if (targetTemperature !== null) {
      payload.targetTemperature = targetTemperature;
    }
    if (targetHeatingTemperature !== null) {
      payload.targetHeatingThresholdTemperature = targetHeatingTemperature;
    }
    if (targetCoolingTemperature !== null) {
      payload.targetCoolingThresholdTemperature = targetCoolingTemperature;
    }
    return await this.throttlePost(async () => {
      const r = await this.httpClient?.post<SetThermostatTemperatureResponse>(
        `devices/${id}`,
        payload
      );

      this.logger.info(`Retrieved ${id}: ${JSON.stringify(r?.data)} ${JSON.stringify(payload)}`);

      return r?.data?.status == "OK" &&
        (targetTemperature == null || r?.data?.setStatus?.targetTemperature == "OK") &&
        (targetHeatingTemperature == null || r?.data?.setStatus?.targetHeatingThresholdTemperature == "OK") &&
        (targetCoolingTemperature == null || r?.data?.setStatus?.targetCoolingThresholdTemperature == "OK");
    });
  }

  // Throttle the number of posts to match the documented API's rate limit of 1 per second with a max number of waiting requests
  private maxQueueCapacity = 5
  private throttle = throttledQueue(1, 1000);
  private async throttlePost(callback: () => Promise<boolean>): Promise<boolean> {
    if (this.maxQueueCapacity <= 0) {
      this.logger.debug(`Rate limit reached`);
      return false;
    }
    this.maxQueueCapacity--;
    return await this.throttle<boolean>(() => {
      this.maxQueueCapacity++;
      return callback();
    });
  }

  destroy() {
    // TODO ?
  }
}

export default StarlingClient;
