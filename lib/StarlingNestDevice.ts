import axios from "axios";
import Homey from "homey";
import Logger from "./Logger";
import StarlingClient, { isStarlingError } from "./StarlingClient";

class StarlingNestDevice extends Homey.Device {
  protected logger = new Logger(this.log, this.error, false);
  protected starlingClient: StarlingClient | undefined;
  protected deviceId: string = "";
  private devicePoller: NodeJS.Timeout | null = null;
  private connectionPoller: NodeJS.Timeout | null = null;

  private async initializeStarlingClient(settings: any): Promise<void> {
    this.starlingClient = new StarlingClient(settings.apiKey, this.logger);

    const initResult = await this.starlingClient?.init();
    if (initResult?.status === "OK") {
      await this.setSettings({ 
        ipAddress: initResult.local_ip
      });

      const statusResult = await this.starlingClient?.getStatus();
      if (statusResult) {
        const permissions = [];
        if (statusResult.permissions.read === true) {
          permissions.push("Read");
        }
        if (statusResult.permissions.write === true) {
          permissions.push("Write");
        }
        if (statusResult.permissions.camera === true) {
          permissions.push("Camera");
        }

        const connectedToNest = statusResult.connectedToNest ? (statusResult.connectedToNest === true ? "Yes" : "No") : settings.connectedToNest;

        await this.setSettings({
          apiVersion: statusResult.apiVersion.toString(),
          apiReady: statusResult.apiReady === true ? "Yes" : "No",
          connectedToNest: connectedToNest,
          appName: statusResult.appName,
          permissions: permissions.join(", "),
          connection_status: `Connected`,
          connection_last_message: "",
          connection_last_communication: this.getSettings().debugMode ? new Date().toISOString() : `Enable debug mode to track last communication`,
        });
      }
    }
  }

  private async ensureInitialized(settings: any): Promise<void> {
    this.logger = new Logger(this.log, this.error, settings.debugMode === true);

    try {
      await this.initializeStarlingClient(settings);
    }
    catch (err: any) {
      this.handleStarlingApiError(err);
    }
  }

  /** Add capabilities if they do not already exist */
  protected async ensureCapabilities(): Promise<void> {}

  private async refreshConnection(): Promise<void> {
    try {
      await this.initializeStarlingClient(this.getSettings());
    }
    catch (err: any) {
      this.handleStarlingApiError(err);
    }
  }

  protected async processDeviceStatus(starlingClient: StarlingClient, deviceId: string): Promise<void> {
  }

  private async getDeviceStatus(): Promise<void> {
    if (this.starlingClient?.isInitialized === true) {
      this.logger.debug(`Getting Device Status for ${this.deviceId}`);

      try {
        await this.processDeviceStatus(this.starlingClient, this.deviceId);
      }
      catch (err: any) {
        this.handleStarlingApiError(err);
      }
    }
  }

  protected async handleStarlingApiError(err: any) {
    await this.setSettings({
      connection_status: `Not Connected`,
      connection_last_message: err?.message ? err.message : "Unknown"
    });

    if (isStarlingError(err)) {
      this.logger.error(`Could not communicate with Starling Home Hub: ${err.code}: ${err.message}`);
    }
    else if (axios.isAxiosError(err)) {
      this.logger.error(`Could not communicate with Starling Home Hub: ${err.code}: ${err.message}`);
    } else {
      this.logger.error(err);
    }
  }

  /**
   * onInit is called when the device is initialized.
   */
  async onInit(): Promise<void> {
    this.logger.info(`Initializing ${this.getName()}`);

    await this.ensureCapabilities();

    const settings = this.getSettings();

    await this.ensureInitialized(settings);

    const data = this.getData();
    this.deviceId = data.id;

    this.logger.info("Device ID", this.deviceId);

    // Set up polling against Starling Home Hub
    const pollingInterval = parseInt(settings.pollingInterval, 10);
    this.logger.debug(`Polling device status every ${pollingInterval} ms`);

    this.devicePoller = setInterval(
      this.getDeviceStatus.bind(this),
      pollingInterval
    );
    this.connectionPoller = setInterval(
      this.refreshConnection.bind(this),
      60000
    );

    this.logger.info(`Initialized ${this.getName()}`);
  }

  async onUninit(): Promise<void> {
    this.starlingClient?.destroy();

    if (this.devicePoller) {
      clearInterval(this.devicePoller);
    }

    if (this.connectionPoller) {
      clearInterval(this.connectionPoller);
    }
  }

  /**
   * onAdded is called when the user adds the device, called just after pairing.
   */
  async onAdded(): Promise<void> {
    this.logger.debug("A StarlingGoogleNest device has been added");
  }

  /**
   * onSettings is called when the user updates the device's settings.
   * @param {object} event the onSettings event data
   * @param {object} event.oldSettings The old settings object
   * @param {object} event.newSettings The new settings object
   * @param {string[]} event.changedKeys An array of keys changed since the previous version
   * @returns {Promise<string|void>} return a custom message that will be displayed
   */
  async onSettings({
    oldSettings,
    newSettings,
    changedKeys,
  }: any): Promise<string | void> {
    if (newSettings.pollingInterval) {
      this.devicePoller = setInterval(
        this.getDeviceStatus,
        newSettings.pollingInterval
      );
    }

    await this.ensureInitialized({
      ...(this.getSettings()),
      ...newSettings,
    });
  }

  /**
   * onRenamed is called when the user updates the device's name.
   * This method can be used this to synchronise the name to the device.
   * @param {string} name The new name
   */
  async onRenamed(name: string): Promise<void> {
    this.logger.debug("A StarlingGoogleNest device was renamed");
  }

  /**
   * onDeleted is called when the user deleted the device.
   */
  async onDeleted(): Promise<void> {
    this.logger.debug("A StarlingGoogleNest device has been deleted");
  }
}

export default StarlingNestDevice;
