import axios from "axios";
import Homey from "homey";
import type PairSession from "homey/lib/PairSession";
import DeviceType from "./DeviceType";
import Logger from "./Logger";
import StarlingClient from "./StarlingClient";
import { StarlingDevice } from "../types/starling";

class StarlingDriver extends Homey.Driver {
  private logger = new Logger(this.log, this.error, false);

  async onInit() {
    this.ensureCards();
  }

  async onPair(session: PairSession): Promise<void> {
    let apiKey = "";
    let ipAddress = "";
    let starlingClient: StarlingClient;

    session.setHandler("api_key", async (data: any) => {
      apiKey = data.apiKey;

      this.logger.debug(`Set API Key to: ${data.apiKey}`);
    });

    session.setHandler('showView', async (view) => {
      if (view == "discover_hub") {
        starlingClient = new StarlingClient(apiKey, this.logger);

        // When navigating from api key, try to get the ip address for the hub
        try {
          const initResult = await starlingClient.init();
          if (initResult?.status === "OK") {
            ipAddress = initResult.local_ip;
            this.logger.info(`Got the ip address for the Starling Hub: ${ipAddress}`);
  
            const statusResult = await starlingClient.getStatus();

            this.logger.info(`Got the API Key permissions: ${JSON.stringify(statusResult)}`);
  
            const hasCorrectPermissions = (
              statusResult?.apiReady === true &&
              statusResult?.permissions.read === true
            );

            if (hasCorrectPermissions) {
              this.logger.debug(`Permissions are correct`);
              await session.emit("ip_address", ipAddress);
            }
            else {
              this.logger.error(`Permissions are incorrect`);
              await session.showView("invalid_permissions");
            }
          } else {
            this.logger.error(`Could not find any hubs`);
            await session.showView("no_hubs_found");
          }
        } catch (err) {
          if (axios.isAxiosError(err)) {
            if (err.response?.status === 401) {
              this.logger.error(`The API Key ${apiKey} is invalid`, err.message);
              await session.showView("invalid_apikey");
            } else  {
              this.logger.error(err.message);
              // TODO: Alert sentry
            }
          } else {
            this.logger.error(err);
            // TODO: Alert sentry
          }
        }
      }
    });

    session.setHandler("list_devices", async () => {
      try {
        const devices = await starlingClient.getDevices(this.getDeviceType());

        this.logger.info(
          `Got ${devices?.length || 0} devices from the Starling Hub`
        );

        const result = devices.map((device: StarlingDevice) => {
          return {
            name: `${device.where} ${device.name}`,
            data: {
              id: device.id,
            },
            settings: {
              apiKey,
              ipAddress,
              serialNumber: device.serialNumber,
              room: device.where,
              building: device.structureName,
            },
          };
        });

        return result;
      } catch (err) {
        if (axios.isAxiosError(err)) {
          this.logger.error(err.message);
        } else {
          this.logger.error(err);
        }
      }
    });
  }

  protected getDeviceType(): DeviceType | null {
    return null;
  }

  /** Add capabilities if they do not already exist */
  protected async ensureCards(): Promise<void> {}

}

export default StarlingDriver;
