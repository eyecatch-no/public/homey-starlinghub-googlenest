import Logger from "./Logger";
import axios, { AxiosInstance } from "axios";
const bonjour = require('bonjour')()
import { PingResult } from "../types/starling";

class StarlingDiscovery {

  private logger: Logger | null = null;
  private defaultAddress: string = 'starling-home-hub.local';
  private address: string = "";
  private lastDiscoveryTime = 0;
  private refreshTime = 60000;
  private searchTime = 5000;
  private fallbackTimeout = 1000;
  private initialDiscoveryRetryDelay = 1000;
  private initialDiscoveryRetries = 5;
  private fallbackRetries = 5;

  private discoveryPoller: NodeJS.Timeout | null = null;

  private static instance: StarlingDiscovery;

  private constructor() {
  }

  public static getInstance(): StarlingDiscovery {
    if (!StarlingDiscovery.instance) {
      StarlingDiscovery.instance = new StarlingDiscovery();
    }
    return StarlingDiscovery.instance;
  }

  public async onInit(logger: Logger): Promise<void> {
    this.logger = logger;
    this.address = this.defaultAddress;
    this.lastDiscoveryTime = 0;
    this.discoveryPoller = setInterval(
      this.refresh.bind(this),
      this.refreshTime
    );
    await this.refresh();

    // Retry inital discovery
    let attempt = 0;
    while (this.lastDiscoveryTime == 0 && attempt < this.initialDiscoveryRetries) {
        await this.refresh();
        attempt++;
        if (this.lastDiscoveryTime == 0) {
            await new Promise(resolve => setTimeout(resolve, this.initialDiscoveryRetryDelay));
        }
    }
  }

  public async onUninit(): Promise<void> {
    if (this.discoveryPoller) {
      clearInterval(this.discoveryPoller);
    }
    this.logger = null; 
  } 

  updateDiscoveryAddress(addr: string) {
    this.lastDiscoveryTime = Date.now();
    this.address = addr;
    this.logger?.info(`Discovered the Starling hub ${addr}`);
  }

  async refresh(): Promise<void> {
    try {
      const addr = await this.findStarlingHub();
      this.updateDiscoveryAddress(addr);
      return;
    } catch (error: any) {
      this.logger?.debug('Primary discovery method failed, trying fallback', error);
    }

    try {
      const addr = await this.findStarlingHubFallback();
      this.updateDiscoveryAddress(addr);
      return;
    } catch (error: any) {
      if (Date.now() - this.lastDiscoveryTime > this.refreshTime * this.fallbackRetries) {
        this.logger?.info(`Error discovering the Starling hub, falling back to ${this.defaultAddress}: `, error);
        this.address = this.defaultAddress;
      } else {
        this.logger?.debug('Error discovering the Starling hub.', error);
      }
    }
  }

  private async findStarlingHub() {
    return new Promise<string>((resolve, reject) => {
      const browser = bonjour.find({ type: 'http' });
      const timer = setTimeout(() => {
          try {
            browser.stop();
          } catch (error) {
            this.logger?.error('Error stopping the mDNS browser:', error);
          }
          reject(new Error('No Starling hubs found, discovery timedout'));
      }, this.searchTime);

      browser.once('up', (service: any) => {
        if (service) {
          this.logger?.debug(`Found service: ${JSON.stringify(service)}`);
          if (service.name == 'Starling-Home-Hub') {
            clearTimeout(timer);
            try {
              browser.stop();
            } catch (error) {
              this.logger?.error('Error stopping the mDNS browser:', error);
            }

            // Filter and format addresses: prioritize IPv4, fallback to IPv6
            const ipv4 = service.addresses.find((addr: string) => this.isIPv4(addr));
            const ipv6 = service.addresses.find((addr: string) => this.isIPv6(addr));

            if (ipv4) {
              resolve(ipv4);
            } else if (ipv6) {
              resolve(this.formatIPv6(ipv6));
            } else {
              reject(new Error('No valid IP addresses found for Starling-Home-Hub'));
            }
          }
        }
      });
    });
  }

  private async findStarlingHubFallback(): Promise<string> {
    return new Promise<string>(async (resolve, reject) => {
      const timeout = setTimeout(() => {
        reject(new Error('No Starling hubs found, request timed out'));
      }, this.fallbackTimeout);

      try {
        const r = await axios.get<PingResult>(
          `http://${this.defaultAddress}/api/starling/v1/ping`
        );

        if (r?.data.status === "OK" && r.data.local_ip) {
          clearTimeout(timeout);
          resolve(r.data.local_ip);
        } else {
          throw new Error('No Starling hubs found');
        }
      } catch (error) {
        clearTimeout(timeout);
        reject(error);
      }
    });
  }

  public getAddress(): string {
    return this.address;
  }

  private isIPv4(address: string): boolean {
    return new RegExp("^[0-9]+\\.[0-9]+\\.[0-9]+\\.[0-9]+$").test(address);
  }

  private isIPv6(address: string): boolean {
    return new RegExp("^[a-fA-F0-9:]+$").test(address);
  }

  private formatIPv6(address: string): string {
    return `[${address}]`;
  }
}

export default StarlingDiscovery;
