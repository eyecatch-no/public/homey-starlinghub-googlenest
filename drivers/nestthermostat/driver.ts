import DeviceType from "../../lib/DeviceType";
import StarlingDriver from "../../lib/StarlingDriver";

class NestThermostatDriver extends StarlingDriver {
  protected async ensureCards(): Promise<void> {
    await super.ensureCards();

    const ecoModeEnabledCondition = this.homey.flow.getConditionCard('eco_mode_enabled');
    ecoModeEnabledCondition.registerRunListener(async (args: any, state: any) => {
      return args.device.getCapabilityValue('eco_mode') === true;
    });
    const fanModeEnabledCondition = this.homey.flow.getConditionCard('fan_mode_enabled');
    fanModeEnabledCondition.registerRunListener(async (args: any, state: any) => {
      return args.device.getCapabilityValue('fan_mode') === true;
    });

    const enableFanAction = this.homey.flow.getActionCard('enable_fan');
    enableFanAction.registerRunListener(async (args: any, state: any) => {
      return args.device.onFanModeListener(true);
    });
    const disableFanAction = this.homey.flow.getActionCard('disable_fan');
    disableFanAction.registerRunListener(async (args: any, state: any) => {
      return args.device.onFanModeListener(false);
    });
    const enableEcoModeAction = this.homey.flow.getActionCard('enable_eco_mode');
    enableEcoModeAction.registerRunListener(async (args: any, state: any) => {
      return args.device.onEcoModeListener(true);
    });
    const disableEcoModeAction = this.homey.flow.getActionCard('disable_eco_mode');
    disableEcoModeAction.registerRunListener(async (args: any, state: any) => {
      return args.device.onEcoModeListener(false);
    });
    const setCoolTemperatureAction = this.homey.flow.getActionCard('set_cool_temperature');
    setCoolTemperatureAction.registerRunListener(async (args: any, state: any) => {
      return args.device.onTargetTemperatureListener("cool", this.getTemperature(args));
    });
    const setHeatTemperatureAction = this.homey.flow.getActionCard('set_heat_temperature');
    setHeatTemperatureAction.registerRunListener(async (args: any, state: any) => {
      return args.device.onTargetTemperatureListener("heat", this.getTemperature(args));
    });
  }

  protected getDeviceType(): DeviceType | null {
    return DeviceType.NestThermostat;
  }

  private getTemperature(args: any): number {
    if (args.units === 'f') {
      return (args.target_temperature - 32) * 5 / 9;
    } else {
      return args.target_temperature;
    }
  }
}

module.exports = NestThermostatDriver;
