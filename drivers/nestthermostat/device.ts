import StarlingNestDevice from "../../lib/StarlingNestDevice";
import StarlingClient from "../../lib/StarlingClient";
import { ThermostatDevice } from "../../types/starling";

class NestThermostatDevice extends StarlingNestDevice {

  private TEMP_MIN = 5;
  private TEMP_MAX = 45;

  protected async ensureCapabilities(): Promise<void> {
    await super.ensureCapabilities();

    this.registerCapabilityListener("fan_mode", this.onFanModeListener.bind(this));
    this.registerCapabilityListener("thermostat_mode", this.onThermostatModeListener.bind(this));
    this.registerCapabilityListener("eco_mode", this.onEcoModeListener.bind(this));
    this.registerCapabilityListener("target_temperature", this.onTargetTemperatureListener.bind(this, "auto"));
    this.registerCapabilityListener("target_temperature.cool", this.onTargetTemperatureListener.bind(this, "cool"));
    this.registerCapabilityListener("target_temperature.heat", this.onTargetTemperatureListener.bind(this, "heat"));

  }

  async onFanModeListener(value: boolean) {
    try {
      if (!(await this.starlingClient?.setThermostatFan(this.deviceId, value))) {
        throw Error(`Unable to ${value ? "enable" : "disable"} the fan`);
      }
    } catch (err: any) {
      this.handleStarlingApiError(err);
      throw err;
    }
  }

  async onThermostatModeListener(value: string) {
    try {
      if (!(await this.starlingClient?.setThermostatHvacMode(this.deviceId, this.getThermostatHvacMode(value)))) {
        throw Error(`Unable to set thermostat mode to ${value}`);
      }
    } catch (err: any) {
      this.handleStarlingApiError(err);
      throw err;
    }
  }

  async onEcoModeListener(value: boolean) {
    try {
      if (!(await this.starlingClient?.setThermostatEcoMode(this.deviceId, value))) {
        throw Error(`Unable to ${value ? "enable" : "disable"} eco mode`);
      }
    } catch (err: any) {
      this.handleStarlingApiError(err);
      throw err;
    }
  } 

  async onTargetTemperatureListener(type: string, value: number) {
    try {
      const r = await this.starlingClient?.getThermostatDevice(this.deviceId);
      if (!r) {
        throw Error('Unable to set target temperature');
      }
      let result = false;
      switch (r.hvacMode) {
        case 'heat':
          if (type == "cool") {
            throw Error('Setting the cool temperature is not supported in heat mode');
          }
          result = await this.starlingClient?.setThermostatTemperature(this.deviceId, value, null, null) ?? false;
          break;
        case 'cool':
          if (type == "heat") {
            throw Error('Setting the heat temperature is not supported in cool mode');
          }
          result = await this.starlingClient?.setThermostatTemperature(this.deviceId, value, null, null) ?? false;
          break;
        case 'heatCool':
          switch (type) {
            case 'heat':
              result = await this.starlingClient?.setThermostatTemperature(this.deviceId, null, value, null) ?? false;
              break;
            case 'cool':
              result = await this.starlingClient?.setThermostatTemperature(this.deviceId, null, null, value) ?? false;
              break;
            case 'auto':
              const comfortRange = this.getComfortRange();
              result = await this.starlingClient?.setThermostatTemperature(this.deviceId, null, value - comfortRange, value + comfortRange) ?? false;
              break;
            default:
              throw new Error(`Unknown temperature type: ${type}`);
          }
          break;
        case 'off':
            throw Error('Setting the temperature is not supported when the thermostat is off');
        default:
          throw new Error(`Unknown HVAC mode: ${r.hvacMode}`);
      }
      if (!result) {
        throw Error('Unable to set the temperature');
      }
    } catch (err: any) {
      this.handleStarlingApiError(err);
      throw err;
    }
  }

  protected async processDeviceStatus(starlingClient: StarlingClient, deviceId: string): Promise<void> {
    const r = await starlingClient?.getThermostatDevice(deviceId);

    if (r) {
      await this.setSettings({
        room: r.where,
        building: r.structureName,
        connection_status: `Connected`,
        connection_last_message: "",
        connection_last_communication: this.getSettings().debugMode ? new Date().toISOString() : `Enable debug mode to track last communication`,
      });

      await this.setCapabilityValue("thermostat_mode", this.getMode(r));
      await this.setCapabilityValue("measure_temperature", r.currentTemperature);
      await this.setCapabilityValue("measure_temperature.device", r.backplateTemperature);
      await this.setCapabilityValue("target_temperature", this.getTempTarget(r));
      await this.setCapabilityValue("target_temperature.cool", this.getCoolTempTarget(r));
      await this.setCapabilityValue("target_temperature.heat", this.getHeatTempTarget(r));
      await this.setCapabilityValue("measure_humidity", r.humidityPercent);
      await this.setCapabilityValue("fan_mode", r.fanRunning ?? false);
      await this.setCapabilityValue("eco_mode", r.ecoMode ?? false);
      await this.setCapabilityValue("hvac_state", this.getHvacState(r));
    }
  }

  private getTempTarget(info: ThermostatDevice): number {
    switch (info.hvacMode) {
      case 'heat':
      case 'cool':
        return info.targetTemperature;
      case 'heatCool':
        return (info.targetHeatingThresholdTemperature + info.targetCoolingThresholdTemperature)/2;
      case 'off':
        return info.targetTemperature;
      default:
        throw new Error(`Unknown HVAC mode: ${info.hvacMode}`);
    }
  }

  private getHeatTempTarget(info: ThermostatDevice): number {
    switch (info.hvacMode) {
      case 'heatCool':
        return info.targetHeatingThresholdTemperature;
      case 'heat':
        return info.targetTemperature;
      case 'off':
      case 'cool':
        return this.TEMP_MAX;
      default:
        throw new Error(`Unknown HVAC mode: ${info.hvacMode}`);
    }
  }

  private getCoolTempTarget(info: ThermostatDevice): number {
    switch (info.hvacMode) {
      case 'heatCool':
        return info.targetCoolingThresholdTemperature;
      case 'cool':
        return info.targetTemperature;
      case 'off':
      case 'heat':
        return this.TEMP_MIN;
      default:
        throw new Error(`Unknown HVAC mode: ${info.hvacMode}`);
    }
  }

  private getMode(info: ThermostatDevice): string {
    switch (info.hvacMode) {
      case 'off':
        return 'off';
      case 'heat':
        return 'heat';
      case 'cool':
        return 'cool';
      case 'heatCool':
        return 'auto';
      default:
        throw new Error(`Unknown HVAC mode: ${info.hvacMode}`);
    }
  }

  private getHvacState(info: ThermostatDevice): string {
    switch (info.hvacState) {
      case 'heating':
        return 'heat';
      case 'cooling':
        return 'cool';
      case 'off':
        if (info.fanRunning) {
          return 'fan';
        } else {
          return 'off';
        }
      default:
        throw new Error(`Unknown HVAC state: ${info.hvacState}`);
    }
  }

  private getThermostatHvacMode(value: string): string {
    switch (value) {
      case 'auto':
        return 'heatCool';
      case 'heat':
        return 'heat';
      case 'cool':
        return 'cool';
      case 'off':
        return 'off';
      default:
        throw new Error(`Unknown HVAC state: ${value}`); 
    } 
  }

  private getComfortRange(): number {
    switch (this.getSetting('comfortZone')) {
      case 'comfort':
        return 0.85;
      case 'balanced':
        return 1.7;
      case 'energy_saver':
      default:
        return 2.55;
    }
  }
}

module.exports = NestThermostatDevice;
