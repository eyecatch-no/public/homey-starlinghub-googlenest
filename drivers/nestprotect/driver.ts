import DeviceType from "../../lib/DeviceType";
import StarlingDriver from "../../lib/StarlingDriver";

class NestProtectDriver extends StarlingDriver {
  protected getDeviceType(): DeviceType | null {
    return DeviceType.NestProtect;
  }
}

module.exports = NestProtectDriver;
