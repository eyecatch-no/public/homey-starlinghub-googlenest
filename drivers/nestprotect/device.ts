import StarlingNestDevice from "../../lib/StarlingNestDevice";
import StarlingClient from "../../lib/StarlingClient";

class NestProtectDevice extends StarlingNestDevice {

  protected async processDeviceStatus(starlingClient: StarlingClient, deviceId: string): Promise<void> {
    const r = await starlingClient?.getProtectDevice(deviceId);

    if (r) {
      await this.setSettings({
        room: r.where,
        building: r.structureName,
        connection_status: `Connected`,
        connection_last_message: "",
        connection_last_communication: this.getSettings().debugMode ? new Date().toISOString() : `Enable debug mode to track last communication`,
      });

      await this.setCapabilityValue("alarm_co", r.coDetected);
      await this.setCapabilityValue("alarm_smoke", r.smokeDetected);
      await this.setCapabilityValue(
        "alarm_battery",
        r.batteryStatus == "low"
      );
      await this.setCapabilityValue("alarm_motion", r.occupancyDetected);
    }
  }

}

module.exports = NestProtectDevice;
