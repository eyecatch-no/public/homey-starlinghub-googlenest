import DeviceType from "../../lib/DeviceType";
import StarlingDriver from "../../lib/StarlingDriver";

class NestTemperatureSensorDriver extends StarlingDriver {
  protected getDeviceType(): DeviceType | null {
    return DeviceType.NestTemperatureSensor;
  }
}

module.exports = NestTemperatureSensorDriver;
