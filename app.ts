import Homey from 'homey';

import Logger from "./lib/Logger";
import StarlingDiscovery from './lib/StarlingDiscovery';

class StarlingGoogleNest extends Homey.App {
  /**
   * onInit is called when the app is initialized.
   */
  async onInit(): Promise<void> {
    const logger = new Logger(this.log, this.error, false);
    await StarlingDiscovery.getInstance().onInit(logger);
    this.log('StarlingGoogleNest has been initialized');
  }

  async onUninit(): Promise<void> {
    await StarlingDiscovery.getInstance().onUninit();
  }
}

module.exports = StarlingGoogleNest;
