# Google Nest with Starling

Adds support for Google Nest devices via the [Starling Home Hub](https://www.starlinghome.io/).

## Getting Started

### 1. Get an API Key
1. Open a browser and navigate to https://starling-home-hub.local/ (You must be on the same network as your Starling Hub for this to work)
2. If asked for a password, enter it.
3. Open the `Starling Developer Connect` section and press `Create API Key`

    ![Starling Home Hub Developer Connect](assets_src/starling_home_hub_config.png)

4. Fill in a name for your key, and enable `R` to view the sensors or `R,W` to change settings. Then press `Create API Key`

    ![Create API Key](assets_src/create_api_key.png)

5. Your API Key will now be displayed in the list, write it down.

### 2. Add the device in Homey
1. Open Homey and install the Starling Home Hub app.
2. Add a device from the Starling app, and use the API Key from step 1 when asked for it.

### 3. Setup Nest Thermostat
1. Open a browser and navigate to https://starling-home-hub.local/ (You must be on the same network as your Starling Hub for this to work)
2. If asked for a password, enter it.
3. Open the `Nest Home and Product Settings` section

    ![Starling Home Hub Nest Home and Product Settings](assets_src/starling_thermostat_config.png)
4. Check Show Eco Mode switch to allow the eco mode to be enabled and disabled
5. Set the fan duration to at least 15 minutes in order to allow the fan to be controlled
